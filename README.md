# Biba Ardoak Odoo Module


## Configure pre-commit

1. Create a virtualenv with Pyenv:

```
$ pyenv virtualenv 3.8.12 odoo-bibaardoak
```

2. Activate virtualenv

```
$ pyenv activate odoo-bibaardoak
```

3. Install `pre-commit` package

```
$ pip install pre-commit
```

4. Install pre-coomit hooks

```
$ pre-commit install
```

5. Create a new commit to execute the hooks
